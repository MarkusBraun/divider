var gulp    = require('gulp');
var plugins = require('gulp-load-plugins')({
    pattern: '*'
});


function sass() {
    return gulp.src('./src/scss/divider.scss')
        .pipe(plugins.sourcemaps.init())
        .pipe(plugins.sass({
            outputStyle: 'compressed'
        }).on('error', plugins.sass.logError))
        .pipe(plugins.postcss([
            plugins.autoprefixer()
        ]))
        .pipe(plugins.sourcemaps.write('./sourcemaps'))
        .pipe(gulp.dest('./dist/css'))
        .pipe(plugins.browserSync.stream({match: '**/*.css'}));
};


function copy() {
    return gulp.src('./src/index.html')
        .pipe(gulp.dest('./dist'));
};


function clean() {
    return plugins.del([
        './dist'
    ]);
};


function serve() {
    plugins.browserSync.init({
        server: {
            baseDir: './dist',
            open: false
        }
    });
    gulp.watch('./src/scss/**/*.scss', sass);
    gulp.watch('./src/index.html', copy);
    gulp.watch('./dist/index.html').on('change', plugins.browserSync.reload);
};

gulp.task('development', gulp.series(clean, sass, copy, serve));
gulp.task('production', gulp.series(clean, sass, copy));
